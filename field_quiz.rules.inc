<?php
/**
 * @file
 * Rules integration for field quiz events.
 */


/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function field_quiz_rules_event_info() {
    return array(
      'field_quiz_question_form_view' => array(
        'label' => t('The user views the question form.'),
        'group' => t('Field Quiz'),
        'variables' => array(
          'user' => array('type' => 'user', 'label' => t('The current user answering the question.')),
          'entity' => array('type' => 'entity', 'label' => t('The question entity where the quiz field is integrated.')),
        ),
      ),
      'field_quiz_answer_correct' => array(
        'label' => t('The user answers the quiz question correct.'),
        'group' => t('Field Quiz'),
        'variables' => array(
            'user' => array('type' => 'user', 'label' => t('The current user answering the question.')),
            'entity' => array('type' => 'entity', 'label' => t('The question entity where the quiz field is integrated.')),
        ),
      ),
      'field_quiz_answer_wrong' => array(
        'label' => t('The user answers the quiz question wrong.'),
        'group' => t('Field Quiz'),
        'variables' => array(
            'user' => array('type' => 'user', 'label' => t('The current user answering the question.')),
            'entity' => array('type' => 'entity', 'label' => t('The question entity where the quiz field is integrated.')),
        ),
      ),
    );
}